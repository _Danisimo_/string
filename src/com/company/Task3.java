package com.company;

import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {

        String[] rename = {"Rename", "last", "three", "symbols"};

        System.out.println(minLengthString("Определить длину самого короткого слова"));
        System.out.println("\t");
        System.out.println(symbolКeplacement(rename,4));
        System.out.println("\t");
        System.out.println(addSpaces("Добавить в строку пробелы ,после знаков !препинания,если они там отсутствуют"));
        System.out.println("\t");
        System.out.println(oneInstance("Can you help me ?"));
        System.out.println("\t");
        System.out.println(countingWord("Подсчитать количество слов во введенной пользователем строке"));
        System.out.println("\t");
        System.out.println(deleteLine("Удалить из строки ее часть с заданной позиции и заданной длины",8,9));
        System.out.println("\t");
        System.out.println(reverse(" Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними."));
        System.out.println("\t");
        System.out.println(deleteLastString("В заданной строке удалить последнее слово"));




    }

    //Определить длину самого короткого слова
    public static int minLengthString(String str){

        String[] st = str.split(" ");
        int min = st[0].length();
        for(int i =1; i < st.length;i++)
        {
            min = st[i].length() <  min ? st[i].length() : min ;
        }
        return min;
    }

    //Заменить последние три символа слов, имеющих заданную длину на символ "$"
    public static String symbolКeplacement(String[] arrayWords, int wordLength){
        for (int i = 0; i < arrayWords.length; i++){
            if(arrayWords[i].length() == wordLength){
                int temp = wordLength - 3;
                arrayWords[i] = arrayWords[i].substring(0, temp) + "$$$";
            }
        }
        return Arrays.toString(arrayWords);
    }

    //   Добавить в строку пробелы после знаков препинания, если они там отсутствуют
    public static String  addSpaces(String str){
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        char[] charIn = new char[]{',','.','?','!',':',';'};

        for(int i=1; i<sb.length()-1;i++){
            for(char ch:charIn){
                if(sb.charAt(i)==ch){
                    if(!sb.substring(i+1,i+2).equals(" ")){
                        sb.insert(i+1,' ');
                    }
                }
            }
        }
        return sb.toString();
    }

    //Оставить в строке только один экземпляр каждого встречающегося символа
    public static String oneInstance(String str){

        char[]chars = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        boolean repChar;

        for (int i = 0; i < chars.length; i++) {

            repChar = false;

            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i] == chars[j]) {

                    repChar = true;
                    break;
                }
            }
            if (!repChar) {
                sb.append(chars[i]);
            }
        }
        return sb.toString();

    }

    //Подсчитать количество слов во введенной пользователем строке
     public static int countingWord(String str){
         int  counting = 0;

         if(str.length() != 0){
             counting++;

             for (int i = 0; i < str.length(); i++) {

                 if(str.charAt(i) == ' '){
                     counting++;
                 }
             }
         }

         return counting;
     }

     //Удалить из строки ее часть с заданной позиции и заданной длины
     public static String deleteLine(String str,int index,int givenLenght){
        StringBuffer sb = new StringBuffer(str);

        sb.replace((index-1),(index-1)+givenLenght,"");

        return sb.toString();
     }

     //Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними
    public static String reverse(String str){
        StringBuffer sb = new StringBuffer(str);
         sb.reverse();
         return sb.toString();
    }

    //В заданной строке удалить последнее слово
     public static String deleteLastString(String str ){

         int lastIndex = str.lastIndexOf(" ");
         str = str.substring(0,lastIndex);

         return str;
     }




}
