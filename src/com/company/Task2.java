package com.company;

import java.io.IOException;

public class Task2 {

    public static void main(String[] args) {
        //numbersToString(5);
        //numbersToString(5.5);
        //stringToNumber("5");
        // stringToDoubble("5.5");


    }

    public static String numbersIntToString(int x ){ return String.valueOf(x);}

    public static String numbersDoubleToString(double x){
        return String.valueOf(x);
    }

    public static int stringToNumberInt(String str){
        int x =0;
        try{
            x = Integer.parseInt(str);
        }catch(Exception e){
            e.printStackTrace();
        }
        return x;
    }

    public static double stringToDoubble(String str){

        try {
            return Double.parseDouble(str);
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

}

