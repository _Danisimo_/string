package com.company.Test;

import java.util.Arrays;

import com.company.Task3;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.assertEquals;

public class TestParametrsTask3 {

    @RunWith(Parameterized.class)
    public static class minLengthStringParameterizeTest {
        private String str;
        private int resValue;

        public minLengthStringParameterizeTest(String str , int resValue){
            this.resValue = resValue;
            this.str = str;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"I don`t know ", 1},
                    {"you help me ", 2},
                    {"Sorry man , I know", 1}
            });
        }

        @Test
        public void minLengthStringParameterizeTest(){
            assertEquals(resValue,new Task3().minLengthString(str));
        }


    }
    /*
    @RunWith(Parameterized.class)
    public static class symbolКeplacementParameterizeTest {
        private String[] str;
        private String[] resValue;
        private int wLenght;

        public symbolКeplacementParameterizeTest(String[] str , String[] resValue , int wLenght){
            this.resValue = resValue;
            this.str = str;
            this.wLenght = wLenght;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {new String[]{"Rename", "last", "three", "symbols"}, 7
                            , new String[]{"Rename", "last", "three", "symb$$$"}},
                    {new String[]{"Rename", "last", "three", "symbols"}, 5
                            , new String[]{"Rename", "last", "th$$$", "symbols"}},
            });
        }

        @Test
        public void symbolКeplacementParameterizeTest(){
            assertEquals(resValue,new Task3().symbolКeplacement(str,wLenght));
        }


    }
*/

    @RunWith(Parameterized.class)
    public static class addSpacesParameterizeTest {
        private String str;
        private String resValue;

        public addSpacesParameterizeTest(String str , String resValue){
            this.resValue = resValue;
            this.str = str;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"I don`t know", "I ?don`t?know!"},
                    {"Can you help me ?", "Can !you ?help .me ?"},
                    {"Sorry man , I know", "Sorry .man ,I ?know"}
            });
        }

        @Test
        public void addSpacesParameterizeTest(){
            assertEquals(resValue,new Task3().addSpaces(str));
        }


    }

    @RunWith(Parameterized.class)
    public static class oneInstanceParameterizeTest {
        private String str;
        private String resValue;

        public oneInstanceParameterizeTest(String str , String resValue){
            this.resValue = resValue;
            this.str = str;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"I don`t know", "Id`t know"},
                    {"Can you help me ?", "Canyouhlpme ?"},
            });
        }

        @Test
        public void oneInstanceParameterizeTest(){
            assertEquals(resValue,new Task3().oneInstance(str));
        }
    }


    @RunWith(Parameterized.class)
    public static class countingWordParameterizeTest {
        private String str;
        private int resValue;

        public countingWordParameterizeTest(String str , int resValue){
            this.resValue = resValue;
            this.str = str;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"I dont know", 3 },
                    {"you help me", 3},
            });
        }

        @Test
        public void countingWordParameterizeTest(){
            assertEquals(resValue,new Task3().countingWord(str));
        }
    }


    @RunWith(Parameterized.class)
    public static class deleteLineParameterizeTest {
        private String str;
        private int resValue;
        private int index;
        private int givenLenght;

        public deleteLineParameterizeTest(String str , int resValue, int index,int givenLenght){
            this.resValue = resValue;
            this.str = str;
            this.index = index;
            this.givenLenght = givenLenght;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"I dont know", 1 , 0 , "I dont know" },
                    {"you help me", 1 , 3 , " help me"},
            });
        }

        @Test
        public void deleteLineParameterizeTest(){
            assertEquals(resValue, new Task3().deleteLine(str,index,givenLenght));
        }
    }


    @RunWith(Parameterized.class)
    public static class reverseParameterizeTest {
        private String str;
        private String resValue;

        public reverseParameterizeTest(String str , String resValue){
            this.resValue = resValue;
            this.str = str;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"I dont know", "wonk tnod I" },
                    {"you help me", "em pleh uoy"},
            });
        }

        @Test
        public void reverseParameterizeTest(){
            assertEquals(resValue,new Task3().reverse(str));
        }
    }

    @RunWith(Parameterized.class)
    public static class deleteLastStrinParameterizeTest {
        private String str;
        private String resValue;

        public deleteLastStrinParameterizeTest(String str , String resValue){
            this.resValue = resValue;
            this.str = str;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"I dont know", "I dont" },
                    {"you help me", "you help"},
            });
        }

        @Test
        public void deleteLastStrinParameterizeTest(){
            assertEquals(resValue,new Task3().deleteLastString(str));
        }
    }




}
