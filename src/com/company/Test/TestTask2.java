package com.company.Test;
import com.company.Task2;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class TestTask2 {

    @Test
    public void numbersIntToString(){
        String arg = "5";

        Assert.assertEquals(arg,Task2.numbersIntToString(5));
    }

    @Test
    public void numberDoubleToString(){
        String arg = "5.5";

        Assert.assertEquals(arg,Task2.numbersDoubleToString(5.5));
    }

    @Test
    public void stringToNumberInt(){
        int  arg = 5;

        Assert.assertEquals(arg,Task2.stringToNumberInt("5"));
    }

    @Test
    public void stringToDouble(){
        double arg = 5.5;

        Assert.assertEquals(arg,Task2.stringToDoubble("5.5"));
    }
}
