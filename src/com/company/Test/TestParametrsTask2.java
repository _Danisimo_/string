package com.company.Test;

import com.company.Task2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;


public class TestParametrsTask2 {


    @RunWith(Parameterized.class)
    public static class numbersIntToStringParameterizeTest{
        private int valueA;
        private String resValue;

        public numbersIntToStringParameterizeTest(int valueA, String resValue){
            this.valueA = valueA;
            this.resValue = resValue;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {1, "1"},
                    {2, "2"},
                    {3, "3"},
                    {4, "4"},
                    {5, "5"}
            });
        }
        @Test
        public void numbersIntToStringParameterizeTest(){
            assertEquals(resValue, new Task2().numbersIntToString(valueA));
        }
    }


    @RunWith(Parameterized.class)
        public static class numbersDoubleToStringParameterixeTest{
            private double valueA;
            private String resValue;

            public numbersDoubleToStringParameterixeTest(double valueA,String resValue) {
                this.valueA = valueA;
                this.resValue = resValue;
            }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest(){
                return Arrays.asList(new Object[][]{
                {1.1, "1.1"},
                {2.2, "2.2"},
                {3.3, "3.3"},
                {4.4, "4.4"},
                {5.5, "5.5"}
            });
        }

        @Test
        public void numbersDoubleToStringParameterixeTest(){
                assertEquals(resValue, new Task2().numbersDoubleToString(valueA));
        }
    }



    @RunWith(Parameterized.class)
    public static class stringToNumberIntParameterixeTest{
        private String valueA;
        private int resValue;

        public stringToNumberIntParameterixeTest(String valueA,int resValue){
            this.valueA= valueA;
            this.resValue = resValue;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest(){
            return Arrays.asList(new Object[][]{
                    {"1.1", 1},
                    {"2.2", 2},
                    {"3.3", 3},
                    {"4.4", 4},
                    {"5.5", 5}
            });
        }

        @Test
        public void stringToNumberIntParameterixeTest(){
            assertEquals(resValue, new Task2().stringToNumberInt(valueA));
        }
    }

    @RunWith(Parameterized.class)
    public static class stringToDoubbleParameterixeTest {
        private String valueA;
        private double resValue;

        public stringToDoubbleParameterixeTest(String valueA, int resValue) {
            this.valueA = valueA;
            this.resValue = resValue;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"1.1", 1.1},
                    {"2.2", 2.2},
                    {"3.3", 3.3},
                    {"4.4", 4.4},
                    {"5.5", 5.5}
            });
        }

        @Test
        public void stringToDoubbleParameterixeTest() {
            assertEquals(resValue, new Task2().stringToDoubble(valueA));
        }
    }

}