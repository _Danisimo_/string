package com.company.Test;
import com.company.Task3;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class TestTask3 {
    @Test
    public void minLengthString(){
        String str = "Определить длину самого короткого слова";
        int arg = 5;

        Assert.assertEquals(arg,Task3.minLengthString(str));
    }

    @Test
    public void addSpaces(){
        String str = "Добавить в строку пробелы ,после знаков !препинания,если они там отсутствуют";
        String arg = "Добавить в строку пробелы , после знаков ! препинания, если они там отсутствуют";

        Assert.assertEquals(arg,Task3.addSpaces(str));
    }

    @Test
    public void oneInstatnce(){
        String str = "Оставить в строке!! только один экземпляр каждого..встречающегося ,,символа";
        String arg = "О!ьнэзпкжд.трчющегя ,символа";

        Assert.assertEquals(arg,Task3.oneInstance(str));
    }

    @Test
    public void countingWord(){
        String str = "Подсчитать количество слов во введенной пользователем строке";
        int arg = 7;

        Assert.assertEquals(arg,Task3.countingWord(str));
    }

    @Test
    public void deleteLine(){
        String str = "Удалить из строки ее часть с заданной позиции и заданной длины";
        int index = 8;
        int givenLenght= 9;

        String arg = "Удалитьи ее часть с заданной позиции и заданной длины";

        Assert.assertEquals(arg,Task3.deleteLine(str,index,givenLenght));
    }

    @Test
    public void reverse (){
        String str =" Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.";
        String arg =".иминделсоп еывреп а ,имывреп ьтатс ынжлод ыловмис еинделсоп .е.т ,укортс ьтунревереП ";

        Assert.assertEquals(arg,Task3.reverse(str));
    }

    @Test
    public void deleteLastString(){
        String str = "В заданной строке удалить последнее слово";
        String arg = "В заданной строке удалить последнее";

        Assert.assertEquals(arg,Task3.deleteLastString(str));
    }

}
